package com.homework.app.controller;

import com.homework.app.model.Homework;
import com.homework.app.payload.request.HomeworkRequestPayload;
import com.homework.app.payload.request.StatusRequestPayload;
import com.homework.app.payload.response.HomeworkResponsePayload;
import com.homework.app.service.HomeworkServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/api/homework")
public class HomeworkController {

    @Autowired
    HomeworkServiceImpl service;

    @ApiOperation(value = "Create Homework", notes = "Create new homework, only authorized to teachers")
    @PostMapping("/")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public Homework addHomework(@RequestBody HomeworkRequestPayload homework){
        return service.addHomework(homework);
    }

    @ApiOperation(value = "Retrieve all Homework", notes = "Retrieve all Homeworks, only authorized to teachers")
    @GetMapping("/")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public List<Homework> getAllHomework(){
        return service.getAllHomeworks();
    }

    @ApiOperation(value = "Retrieve homework by id", notes = "Retrieve homework by its id, only authorized to teachers")
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public Optional<Homework> getHomeworkById(@PathVariable String id){
        return service.getHomeworkById(id);
    }

    @ApiOperation(value = "Retrieve homeworks of logged in student", notes = "Retrieve all homeworks of the currently logged in student, only authorized to students")
    @GetMapping("/student")
    @PreAuthorize("hasAnyAuthority('student')")
    public List<Homework> getHomeworksOfLoggedInStudent(){
        return service.getHomeworksOfLoggedInStudent();
    }

    @ApiOperation(value = "Retrieve homeworks created by logged in teacher", notes = "Retrieve all homeworks created by the currently logged in teacher, only authorized to teacher")
    @GetMapping("/teacher")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public List<Homework> getHomeworksOfLoggedInTeacher(){
        return service.getHomeworksOfLoggedInTeacher();
    }

    @ApiOperation(value = "Retrieve homeworks by student username", notes = "Retrieve all homeworks by assigned student username")
    @GetMapping("/student/{username}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public List<Homework> getHomeworkByStudentUsername(@PathVariable String username){
        return service.getHomeworksByStudentUsername(username);
    }

    @ApiOperation(value = "Update homeworks by id", notes = "Update/change homeworks by its id, only authorized to teachers")
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public HomeworkResponsePayload changeHomeworkById(@RequestBody HomeworkRequestPayload homework, @PathVariable String id){
        return service.changeHomeworkById(homework, id);
    }

    @ApiOperation(value = "Update homework's status by id", notes = "Update homework's status by its id, only authorized to students")
    @PutMapping("/status/{id}")
    @PreAuthorize("hasAnyAuthority('student')")
    public HomeworkResponsePayload changeHomeworkStatus(@PathVariable String id, @RequestBody StatusRequestPayload status){
        return service.changeHomeworkStatus(status, id);
    }

    @ApiOperation(value = "Delete homework's status by id", notes = "Delete homework's status by its id, only authorized to teachers")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public Homework deleteHomework(@PathVariable String id){
        return service.deleteHomework(id);
    }

}
