package com.homework.app.controller;

import com.homework.app.model.User;
import com.homework.app.payload.request.UserRequestPayload;
import com.homework.app.payload.response.UserResponsePayLoad;
import com.homework.app.service.UserServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/users")
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @ApiOperation(value = "Retrieve all users", notes = "Retrieve all users, only authorized to teachers")
    @GetMapping("/{role}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public List<User> getAllUsersByRole(@PathVariable String role){
        return userService.getAllUsersByRole(role);
    }

    @ApiOperation(value = "Retrieve logged in user", notes = "Retrieve details of currently logged in user")
    @GetMapping("/user")
    @PreAuthorize("hasAnyAuthority('teacher','student')")
    public User getCurrentlyLoggedInUser(){
        return userService.getCurrentlyLoggedInUser();
    }

    @ApiOperation(value = "Retrieve user by id", notes = "Retrieve user by id, only authorized to teachers")
    @GetMapping("/user/{id}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public Optional<User> getUserById(@PathVariable String id){
        return userService.getUserById(id);
    }

    @ApiOperation(value = "Create new user", notes = "Create new user")
    @PostMapping("/signup/{role}")
    public UserResponsePayLoad addUser(@PathVariable String role, @RequestBody UserRequestPayload user){
        return userService.addUser(user, role);
    }

    @ApiOperation(value = "Update user", notes = "Update an existing user")
    @PutMapping("user/{id}")
    @PreAuthorize("hasAnyAuthority('teacher','student')")
    public UserResponsePayLoad updateUser(@PathVariable String id, @RequestBody UserRequestPayload user){
        return userService.updateUser(id, user);
    }

    @ApiOperation(value = "Delete user", notes = "Delete an existing user, only authorized to teachers")
    @DeleteMapping("user/{id}")
    @PreAuthorize("hasAnyAuthority('teacher')")
    public UserResponsePayLoad deleteUser(@PathVariable String id){
        return userService.deleteUser(id);
    }

    @GetMapping("/test/username/{username}")
    public User testMeth(@PathVariable String username){
        return userService.getUserByUsername(username);
    }
    @GetMapping("/test/role/{role}")
    public List<User> testMethRole(@PathVariable String role){
        return userService.getAllUsersByRole(role);
    }

}
