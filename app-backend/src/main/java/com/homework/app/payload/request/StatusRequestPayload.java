package com.homework.app.payload.request;

public class StatusRequestPayload {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
