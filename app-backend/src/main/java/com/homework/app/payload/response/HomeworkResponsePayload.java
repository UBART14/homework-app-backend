package com.homework.app.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class HomeworkResponsePayload {
    private String id;
    private String title;
    private String objectives;
    private Date createdAt;
    private String status;
    private Date deadline;
    private Date lastUpdatedAt;
    private String assignedBy;
    private String assignedTo;
    private String errorMessage;

    public HomeworkResponsePayload() {
    }

    public HomeworkResponsePayload(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
