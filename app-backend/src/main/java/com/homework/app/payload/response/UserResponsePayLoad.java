package com.homework.app.payload.response;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponsePayLoad {
    private String id;
    private String name;
    private String username;
    private String role;
    private String errorMessage;

    public UserResponsePayLoad(){
    }

    public UserResponsePayLoad(String errorMessage){
        this.errorMessage = errorMessage;
    }
}
