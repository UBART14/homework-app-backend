package com.homework.app.respository;

import com.homework.app.model.Homework;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeworkRepository extends MongoRepository<Homework, String> {
    List<Homework> findByAssignedTo(String assignedTo);
    List<Homework> findByAssignedBy(String assignedBy);
}
