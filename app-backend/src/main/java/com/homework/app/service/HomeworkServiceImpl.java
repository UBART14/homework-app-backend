package com.homework.app.service;

import com.homework.app.model.Homework;
import com.homework.app.payload.request.HomeworkRequestPayload;
import com.homework.app.payload.request.StatusRequestPayload;
import com.homework.app.payload.response.HomeworkResponsePayload;
import com.homework.app.respository.HomeworkRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class HomeworkServiceImpl implements IHomeworkService {

    private final Logger logger = LoggerFactory.getLogger(HomeworkServiceImpl.class);

    @Autowired
    private HomeworkRepository hwRepository;

    public Optional<Homework> getHomeworkById(String id){
        return hwRepository.findById(id);
    }

    public List<Homework> getAllHomeworks(){
        return hwRepository.findAll();
    }

    public Homework addHomework(HomeworkRequestPayload homeworkRequestPayload){
        Homework newHomework = convertHomeworkRequestPayloadToHomework(homeworkRequestPayload);
        newHomework.setAssignedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        newHomework.setLastUpdatedAt(new Date());
        newHomework.setCreatedAt(new Date());
        newHomework.setStatus("Not Finished");
        logger.info("Homework added successfully.");
        return hwRepository.save(newHomework);
    }

    public HomeworkResponsePayload changeHomeworkStatus(StatusRequestPayload status, String id){
        Optional<Homework> homework= hwRepository.findById(id);
        if(homework.isPresent()){
            Homework updatedHomework = homework.get();
            updatedHomework.setStatus(status.getStatus());
            logger.info("Homework's status changed successfully.");
            return convertHomeworkToHomeworkResponsePayload(hwRepository.save(updatedHomework));
        } else {
            logger.info("No homework found with the provided ID.");
            return new HomeworkResponsePayload("No homework found with the provided ID.");
        }
    }

    public HomeworkResponsePayload changeHomeworkById(HomeworkRequestPayload homeworkRequestPayload, String id){

        Homework newHomework = convertHomeworkRequestPayloadToHomework(homeworkRequestPayload);
        Optional<Homework> currentHomework = hwRepository.findById(id);
        if(currentHomework.isPresent()){
            Homework updatingHomework = currentHomework.get();
            updatingHomework.setLastUpdatedAt(new Date());

            if (newHomework.getTitle() != null){
                updatingHomework.setTitle(newHomework.getTitle());
            }
            if (newHomework.getDeadline() != null){
                updatingHomework.setDeadline(newHomework.getDeadline());
            }
            if (newHomework.getObjectives() != null){
                updatingHomework.setObjectives(newHomework.getObjectives());
            }
            if (newHomework.getAssignedBy() != null){
                updatingHomework.setAssignedBy(newHomework.getAssignedBy());
            }
            if (newHomework.getAssignedTo() != null){
                updatingHomework.setAssignedTo(newHomework.getAssignedTo());
            }
            logger.info("Homework changed successfully.");
            return convertHomeworkToHomeworkResponsePayload(hwRepository.save(updatingHomework));
        } else {
            logger.info("No homework found with the provided ID.");
            return new HomeworkResponsePayload("No homework found with the provided ID.");
        }
    }

    public List<Homework> getHomeworksByStudentUsername(String username){
        logger.info("Getting Homeworks by student Username");
        return hwRepository.findByAssignedTo(username);
    }

    public List<Homework> getHomeworksOfLoggedInStudent(){
        logger.info("Getting Homeworks of currently logged in student");
        return hwRepository.findByAssignedTo(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public List<Homework> getHomeworksOfLoggedInTeacher(){
        logger.info("Getting Homeworks of currently logged in teacher");
        return hwRepository.findByAssignedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public Homework deleteHomework(String id) {
        Optional<Homework> optionalHomework = hwRepository.findById(id);
        if (optionalHomework.isPresent()){
            Homework deletedHomework = optionalHomework.get();
            hwRepository.delete(deletedHomework);
            logger.info("homework deleted.");
            return deletedHomework;
        } else {
            logger.info("homework was not found");
            return null;
        }
    }

    public static HomeworkResponsePayload convertHomeworkToHomeworkResponsePayload(Homework homework) {
        HomeworkResponsePayload homeworkResponsePayload = new HomeworkResponsePayload();
        homeworkResponsePayload.setId(homework.getId());
        homeworkResponsePayload.setTitle(homework.getTitle());
        homeworkResponsePayload.setStatus(homework.getStatus());
        homeworkResponsePayload.setObjectives(homework.getObjectives());
        homeworkResponsePayload.setLastUpdatedAt(homework.getLastUpdatedAt());
        homeworkResponsePayload.setCreatedAt(homework.getCreatedAt());
        homeworkResponsePayload.setDeadline(homework.getDeadline());
        homeworkResponsePayload.setAssignedTo(homework.getAssignedTo());
        homeworkResponsePayload.setAssignedBy(homework.getAssignedBy());
        return homeworkResponsePayload;
    }
    public static Homework convertHomeworkRequestPayloadToHomework(HomeworkRequestPayload homeworkRequestPayload) {
        Homework homework = new Homework();
        homework.setId(homeworkRequestPayload.getpId());
        homework.setTitle(homeworkRequestPayload.getpTitle());
        homework.setStatus(homeworkRequestPayload.getpStatus());
        homework.setObjectives(homeworkRequestPayload.getpObjectives());
        homework.setLastUpdatedAt(homeworkRequestPayload.getpLastUpdatedAt());
        homework.setCreatedAt(homeworkRequestPayload.getpCreatedAt());
        homework.setDeadline(homeworkRequestPayload.getpDeadline());
        homework.setAssignedTo(homeworkRequestPayload.getpAssignedTo());
        homework.setAssignedBy(homeworkRequestPayload.getpAssignedBy());
        return homework;
    }
}

