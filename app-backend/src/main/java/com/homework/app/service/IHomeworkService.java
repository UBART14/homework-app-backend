package com.homework.app.service;

import com.homework.app.model.Homework;
import com.homework.app.payload.request.HomeworkRequestPayload;
import com.homework.app.payload.response.HomeworkResponsePayload;

import java.util.List;
import java.util.Optional;

public interface IHomeworkService {

    Optional<Homework> getHomeworkById(String id);
    List<Homework> getAllHomeworks();
    Homework addHomework(HomeworkRequestPayload homeworkRequestPayload);
    HomeworkResponsePayload changeHomeworkById(HomeworkRequestPayload homeworkRequestPayload, String id);
    Homework deleteHomework(String id);
    List<Homework> getHomeworksByStudentUsername(String username);
}
