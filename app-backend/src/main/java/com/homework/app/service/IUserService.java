package com.homework.app.service;

import com.homework.app.model.User;
import com.homework.app.payload.request.UserRequestPayload;
import com.homework.app.payload.response.UserResponsePayLoad;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    public UserResponsePayLoad addUser(UserRequestPayload userRequestPayload, String role);
    public List<User> getAllUsersByRole(String role);
    public Optional<User> getUserById(String id);
    public UserResponsePayLoad updateUser(String id, UserRequestPayload userRequestPayload);
    public UserResponsePayLoad deleteUser(String id);

}
