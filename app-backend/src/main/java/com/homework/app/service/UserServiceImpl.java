package com.homework.app.service;

import com.homework.app.model.User;
import com.homework.app.payload.request.UserRequestPayload;
import com.homework.app.payload.response.UserResponsePayLoad;
import com.homework.app.respository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements IUserService, UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserResponsePayLoad addUser(UserRequestPayload userRequestPayload, String role){
        if(isUsernameTaken(userRequestPayload.getpUsername())){
            logger.info("Username is taken.");
            return new UserResponsePayLoad("The username you entered is taken.");
        } else {
            User user = convertUserRequestPayloadToUser(userRequestPayload);
            logger.info("User is being created.");
            return convertUserToUserResponsePayload(userRepository.save(user));
        }
    }

    public Optional<User> getUserById(String id){
        return userRepository.findById(id);
    }

    public UserResponsePayLoad updateUser(String id, UserRequestPayload userRequestPayload){

        User user = new User();
        user.setName(userRequestPayload.getpName());
        user.setPassword(userRequestPayload.getpPassword());

        Optional<User> currentUser = userRepository.findById(id);
        if (currentUser.isPresent()){
            User updatingUser = currentUser.get();
            if(user.getName() != null){
                updatingUser.setName(user.getName());
            }
            if(user.getPassword() != null){
                updatingUser.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            logger.info("User updated added successfully");
            return convertUserToUserResponsePayload(userRepository.save(updatingUser));
        } else {
            logger.info("User was not found");
            return new UserResponsePayLoad("userId not found");
        }
    }

    public UserResponsePayLoad deleteUser(String id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User deletedUser = optionalUser.get();
            userRepository.delete(deletedUser);
            logger.info("User updated added successfully");
            return convertUserToUserResponsePayload(deletedUser);
        } else {
            logger.info("User was not found");
            return new UserResponsePayLoad("userId not found");
        }
    }

    public User getUserByUsername(String username){
        logger.info("Getting user by username");
        return userRepository.findByUsername(username);
    }

    public List<User> getAllUsersByRole(String role){
        logger.info("Getting users by role");
        return userRepository.findByRole(role);
    }

    public boolean isUsernameTaken(String username){
        logger.info("Checking if username is taken");
        if (userRepository.findByUsername(username) == null){
            return false;
        } else {
            return true;
        }
    }

    public User getCurrentlyLoggedInUser() {
        logger.info("Getting currently logged in user");
        return userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if(user == null){
            logger.info("User was not found");
            throw new UsernameNotFoundException(username + " user not found");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole()));

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    public static UserResponsePayLoad convertUserToUserResponsePayload(User user){
        UserResponsePayLoad userResponsePayLoad = new UserResponsePayLoad();
        userResponsePayLoad.setId(user.getId());
        userResponsePayLoad.setName(user.getName());
        userResponsePayLoad.setUsername(user.getUsername());
        userResponsePayLoad.setRole(user.getRole());
        return userResponsePayLoad;
    }

    public static User convertUserRequestPayloadToUser(UserRequestPayload userResponsePayLoad){
        User user = new User();
        user.setId(userResponsePayLoad.getpId());
        user.setName(userResponsePayLoad.getpName());
        user.setUsername(userResponsePayLoad.getpUsername());
        user.setRole(userResponsePayLoad.getpRole());
        user.setPassword(userResponsePayLoad.getpPassword());
        return user;
    }
}
