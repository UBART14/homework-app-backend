package com.homework.app.service;


import com.homework.app.model.Homework;
import com.homework.app.payload.request.HomeworkRequestPayload;
import com.homework.app.payload.request.StatusRequestPayload;
import com.homework.app.payload.response.HomeworkResponsePayload;
import com.homework.app.respository.HomeworkRepository;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
class HomeworkServiceImplTest {

    private HomeworkServiceImpl service;
    private Homework homework;
    private HomeworkRequestPayload homeworkRequestPayload;

    @Mock
    private HomeworkRepository homeworkRepositoryTest;

    @BeforeEach
    void setUp(){
        service = new HomeworkServiceImpl(homeworkRepositoryTest);
        homework = new Homework();
        homework.setId("test_id");
        homework.setTitle("test_title");
        homework.setObjectives("test_objectives");
        homework.setAssignedTo("test_assignedTo");
        homework.setAssignedBy("test_assignedBy");
        homework.setCreatedAt(new Date());
        homework.setDeadline(new Date());
        homework.setLastUpdatedAt(new Date());

        homeworkRequestPayload = new HomeworkRequestPayload();
        homeworkRequestPayload.setpId("test_id");
        homeworkRequestPayload.setpTitle("test_title");
        homeworkRequestPayload.setpObjectives("test_objectives");
        homeworkRequestPayload.setpAssignedTo("test_assignedTo");
        homeworkRequestPayload.setpStatus("test_status");
        homeworkRequestPayload.setpAssignedBy("test_assignedBy");
        homeworkRequestPayload.setpCreatedAt(new Date());
        homeworkRequestPayload.setpDeadline(new Date());
        homeworkRequestPayload.setpLastUpdatedAt(new Date());
    }

    @Test
    @DisplayName("This tests getHomeworkById service method")
    void getHomeworkByIdTest(){
        doReturn(Optional.of(homework)).when(homeworkRepositoryTest).findById("test_id");
        assertEquals(Optional.of(homework), service.getHomeworkById("test_id"));
    }


    @Test
    @DisplayName("This tests getAllHomework service method")
    void getAllHomeworkTest(){
        doReturn(Arrays.asList(homework)).when(homeworkRepositoryTest).findAll();
        assertEquals(Arrays.asList(homework), service.getAllHomeworks());
    }

    @Test
    @DisplayName("This tests addHomework service method")
    void addHomeworkServiceMethodTest(){
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn("username").when(authentication).getName();
        SecurityContextHolder.setContext(securityContext);

        doReturn(homework).when(homeworkRepositoryTest).save(any(Homework.class));
        assertEquals(homework, service.addHomework(homeworkRequestPayload));
    }

    @Test
    @DisplayName("This tests home status change when homework presents")
    void changeHomeworkStatusIfHomeworkPresentTest(){
        StatusRequestPayload statusRequestPayload = new StatusRequestPayload();
        statusRequestPayload.setStatus("test_status");
        doReturn(Optional.ofNullable(homework)).when(homeworkRepositoryTest).findById("test_id");
        doReturn(homework).when(homeworkRepositoryTest).save(homework);
        HomeworkResponsePayload response = service.changeHomeworkStatus(statusRequestPayload,"test_id");
        assertAll("Should return HomeworkResponsePayload type object with homeworks's data",
                () -> assertEquals("test_id", response.getId()),
                () -> assertEquals("test_status", response.getStatus()),
                () -> assertEquals(null, response.getErrorMessage())
        );
    }

    @Test
    @DisplayName("This tests home status change when homework presents")
    void getHomeworksByStudentUsernameTest(){
        doReturn(Arrays.asList(homework)).when(homeworkRepositoryTest).findByAssignedTo("test_username");
        assertEquals(Arrays.asList(homework), service.getHomeworksByStudentUsername("test_username"));
    }

    @Test
    @DisplayName("This tests home status change when homework is not present")
    void changeHomeworkStatusIfHomeworkNotPresentTest(){
        StatusRequestPayload statusRequestPayload = new StatusRequestPayload();
        statusRequestPayload.setStatus("test_status");
        doReturn(Optional.ofNullable(null)).when(homeworkRepositoryTest).findById("test_id");
        HomeworkResponsePayload homeworkResponsePayloadError = service.changeHomeworkStatus(statusRequestPayload,"test_id");
        assertEquals("No homework found with the provided ID.", homeworkResponsePayloadError.getErrorMessage());
    }


    @Test
    @DisplayName("This tests changeHomeworkById service method when homework is present")
    void changeHomeworkByIdIfHomeworkPresentTestTest(){
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn("username").when(authentication).getName();
        SecurityContextHolder.setContext(securityContext);

        doReturn(homework).when(homeworkRepositoryTest).save(any(Homework.class));
        assertEquals(homework, service.addHomework(homeworkRequestPayload));
        doReturn(Optional.ofNullable(homework)).when(homeworkRepositoryTest).findById("test_id");
        doReturn(homework).when(homeworkRepositoryTest).save(any(Homework.class));
        HomeworkResponsePayload response = service.changeHomeworkById(homeworkRequestPayload,"test_id");
        assertAll("Should return HomeworkResponsePayload type object with homeworks's data",
                () -> assertEquals("test_id", response.getId()),
                () -> assertEquals(null, response.getErrorMessage())
        );
    }

    @Test
    @DisplayName("This tests changeHomeworkById service method when homework is not present")
    void changeHomeworkByIdIfHomeworkNotPresentTestTest(){
        doReturn(Optional.ofNullable(null)).when(homeworkRepositoryTest).findById("test_id");
        HomeworkResponsePayload homeworkResponsePayloadError = service.changeHomeworkById(homeworkRequestPayload,"test_id");
        assertEquals("No homework found with the provided ID.", homeworkResponsePayloadError.getErrorMessage());

    }

    @Test
    @DisplayName("This tests getting homeworks of currently logged in student")
    void getHomeworksOfLoggedInStudentTest(){
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn("username").when(authentication).getName();
        SecurityContextHolder.setContext(securityContext);

        doReturn(Arrays.asList(homework)).when(homeworkRepositoryTest).findByAssignedTo("username");
        assertEquals(Arrays.asList(homework), service.getHomeworksOfLoggedInStudent());
    }

    @Test
    @DisplayName("This tests getting homeworks of currently logged in teacher")
    void getHomeworksOfLoggedInTeacherTest(){
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn("username").when(authentication).getName();
        SecurityContextHolder.setContext(securityContext);

        doReturn(Arrays.asList(homework)).when(homeworkRepositoryTest).findByAssignedBy("username");
        assertEquals(Arrays.asList(homework), service.getHomeworksOfLoggedInTeacher());
    }

    @Test
    @DisplayName("This deleteHomework service when homework exists")
    void deleteHomeworkIfExistsTest(){
        doReturn(Optional.ofNullable(homework)).when(homeworkRepositoryTest).findById(any(String.class));
        assertEquals(homework, service.deleteHomework("test_id"));
    }

    @Test
    @DisplayName("This deleteHomework service when homework does not exist")
    void deleteHomeworkIfNotExistTest(){
        doReturn(Optional.ofNullable(null)).when(homeworkRepositoryTest).findById(any(String.class));
        assertEquals(null, service.deleteHomework("test_id"));
    }

}