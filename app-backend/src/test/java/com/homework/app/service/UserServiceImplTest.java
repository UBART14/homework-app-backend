package com.homework.app.service;

import com.homework.app.model.User;
import com.homework.app.payload.request.UserRequestPayload;
import com.homework.app.payload.response.UserResponsePayLoad;
import com.homework.app.respository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceImplTest {

    private UserServiceImpl service;
    private User user;
    private UserRequestPayload userRequestPayload;

    @Mock
    private UserRepository userRepositoryTest;
    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp(){
        service = new UserServiceImpl(userRepositoryTest, passwordEncoder);
        user = new User();
        user.setId("test_id");
        user.setName("test_name");
        user.setUsername("test_username");
        user.setPassword("test_password");
        user.setRole("teacher");

        userRequestPayload = new UserRequestPayload();
        userRequestPayload.setpId(user.getId());
        userRequestPayload.setpName(user.getName());
        userRequestPayload.setpUsername(user.getUsername());
        userRequestPayload.setpRole(user.getRole());
        userRequestPayload.setpPassword(user.getPassword());
    }

    @Test
    @DisplayName("This tests add user when username is not taken")
    void addUserWhenUsernameIsNotTakenTest() {
        when(service.getUserByUsername(any(String.class))).thenReturn(null);
        when(userRepositoryTest.save(any(User.class))).thenReturn(user);
        UserResponsePayLoad response = service.addUser(userRequestPayload, "teacher");
        assertAll("Should return UserResponsePayload type object with user's data",
                () -> assertEquals("test_id", response.getId()),
                () -> assertEquals("test_username", response.getUsername()),
                () -> assertEquals(null, response.getErrorMessage())
        );
    }

    @Test
    @DisplayName("This tests add user when username is taken")
    void addUserWhenUsernameIsTakenTest() {
        when(service.getUserByUsername(any(String.class))).thenReturn(user);
        UserResponsePayLoad response = service.addUser(userRequestPayload, "teacher");
        assertEquals("The username you entered is taken.", response.getErrorMessage());
    }

    @Test
    @DisplayName("This tests username availability when username is taken")
    void isUsernameTakenWhenUsernameTakenTest() {
        doReturn(user).when(userRepositoryTest).findByUsername(any(String.class));
        assertTrue(service.isUsernameTaken("test_username"));
    }

    @Test
    @DisplayName("This tests username availability when username is not taken")
    void isUsernameTakenWhenUsernameIsNotTakenTest() {
        doReturn(null).when(userRepositoryTest).findByUsername(any(String.class));
        assertFalse(service.isUsernameTaken("test_username"));
    }

    @Test
    @DisplayName("This tests get all users by role")
    void getAllUsersByRoleTest() {
        doReturn(Arrays.asList(user)).when(userRepositoryTest).findByRole(any(String.class));
        assertEquals(Arrays.asList(user), service.getAllUsersByRole("teacher"));
    }

    @Test
    @DisplayName("This tests get currently logged in user")
    void getCurrentlyLoggedInUserTest() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn("username").when(authentication).getName();
        SecurityContextHolder.setContext(securityContext);
        doReturn(user).when(userRepositoryTest).findByUsername("username");
        assertEquals(user, service.getCurrentlyLoggedInUser());
    }

    @Test
    @DisplayName("This tests get user by id")
    void getUserByIdTest() {
        doReturn(Optional.of(user)).when(userRepositoryTest).findById(any(String.class));
        assertEquals(Optional.of(user), service.getUserById("test_id"));
    }

    @Test
    @DisplayName("This tests get user by username")
    void getUserByUsernameTest() {
        when(userRepositoryTest.findByUsername(any(String.class))).thenReturn(user);
        assertEquals(user, service.getUserByUsername("test_username"));
    }

    @Test
    @DisplayName("This tests update user if user exists")
    void updateUserIfUserExistTest() {
        doReturn(Optional.ofNullable(user)).when(userRepositoryTest).findById(any(String.class));
        doReturn(user).when(userRepositoryTest).save(any(User.class));
        UserResponsePayLoad response = service.updateUser("test_id", userRequestPayload);
        assertAll("Should return UserResponsePayload type object with user's data",
                () -> assertEquals("test_id", response.getId()),
                () -> assertEquals("test_username", response.getUsername()),
                () -> assertEquals(null, response.getErrorMessage())
        );
    }

    @Test
    @DisplayName("This tests update user if user does not exist")
    void updateUserIfUserNotExistTest() {
        doReturn(Optional.ofNullable(null)).when(userRepositoryTest).findById(any(String.class));
        assertEquals("userId not found", service.updateUser("test_id", userRequestPayload).getErrorMessage());
    }

    @Test
    @DisplayName("This tests delete user if user exists")
    void deleteUserIfUserExistTest() {
        doReturn(Optional.ofNullable(user)).when(userRepositoryTest).findById(any(String.class));
        UserResponsePayLoad response = service.deleteUser("test_id");
        assertAll("Should return UserResponsePayload type object with user's data",
                () -> assertEquals("test_id", response.getId()),
                () -> assertEquals("test_username", response.getUsername()),
                () -> assertEquals(null, response.getErrorMessage())
        );
    }


    @Test
    @DisplayName("This tests delete user if user does not exist")
    void deleteUserIfUserNotExistTest() {
        doReturn(Optional.ofNullable(null)).when(userRepositoryTest).findById(any(String.class));
        assertEquals("userId not found", service.deleteUser("test_id").getErrorMessage());
    }

    @Test
    @DisplayName("This tests loadUserByUsername method")
    void loadUserByUsername(){
        org.springframework.security.core.userdetails.User userDetailsUser = new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                Arrays.asList(new SimpleGrantedAuthority(user.getRole())));
        when(userRepositoryTest.findByUsername(any(String.class))).thenReturn(user);
        assertEquals(userDetailsUser, service.loadUserByUsername("test_user"));
    }
}